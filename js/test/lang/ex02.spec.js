function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  // if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
  // Esse regex de email é muito restritivo, qualquer domínio != .com não será
  // validado. Em seu lugar, vou usar o regex de emailregex.com, que é mais abrangente
  if (
    !email.match(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
  ) {
    throw new Error('validation error: invalid email format');
  }

  // adicionei um ^ ao começo e um $ ao final, pois o regex dava match se houvesse uma substring válida na senha
  if (!password.match(/^[a-z0-9]{6,20}$/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  test('empty user string', () => {
    expect(() => {
      createUser('', 'test@test.com', 'abc123', 'abc123');
    }).toThrow(Error);
  });

  test('big user string', () => {
    expect(() => {
      createUser(
        'Esse usuário tem muitos espaços que são permitidos. Porém o createUser vai falhar por causa do seu tamanho',
        'test@test.com',
        'abc123',
        'abc123'
      );
    }).toThrow(Error);
  });

  test('invalid email', () => {
    expect(() => {
      createUser(
        'Usuário',
        'usuario com espaço@meusite.io',
        'abc123',
        'abc123'
      );
    }).toThrow(Error);
  });

  test('password too short', () => {
    expect(() => {
      createUser('Usuário', 'usuario@meusite.com', 'abc', 'abc');
    }).toThrow(Error);
  });

  // esse teste falhava antes de corrigir o regex que verifica os caracteres da senha
  test('password with invalid characters', () => {
    expect(() => {
      createUser(
        'Usuário',
        'usuario@meusite.com',
        '!nãopodeemoji😢',
        '!nãopodeemoji😢'
      );
    }).toThrow(Error);
  });

  test("password doesn't match confirmation", () => {
    expect(() => {
      createUser('Usuário', 'usuario@meusite.com', 'abc1234', '1234abc');
    }).toThrow(Error);
  });

  // esse teste falhava antes de corrigir o regex que verifica o email
  test('valid email that did not work before', () => {
    expect(() => {
      createUser('Usuário', 'usuario@meusite.io', 'abc123', 'abc123');
    }).not.toThrow(Error);
  });

  test('valid user creation', () => {
    expect(() => {
      createUser('user', 'teste@teste.com', 'abc1234', 'abc1234');
    }).not.toThrow(Error);
  });
});
