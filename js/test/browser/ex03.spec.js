const test = require('ava');

function scaffoldStructure() {
  const uList = document.createElement('ul');

  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  data.forEach(function(item, index, arr) {
    const listItem = document.createElement('li');
    listItem.innerHTML = `<strong class="name">${item['name']}</strong>`;
    listItem.innerHTML += ' - ';
    listItem.innerHTML += `<a href="mailto:${item['email']}" class="email">${item['email']}</a>`;
    uList.appendChild(listItem);
  });

  document.body.appendChild(uList);
}

test('the creation of a page from scratch', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, data.length);
});
